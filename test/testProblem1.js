const fs = require('fs');

let { toCreateFolderWithFile, toDeleteFile } = require("../problem1")

// Usage example
const folderPath = './RandomFiles';
const fileName = 'File';
const fileContent = 'I have created random jason files';

toCreateFolderWithFile(folderPath, fileName, fileContent, 5, toDeleteFile);
