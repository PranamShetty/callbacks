const fs = require('fs');

function toCreateFolderWithFile(path, file, content, numberOfFiles,callback) {
  const filePath = `${path}/${file}`;
  let jsonContent = JSON.stringify(content);

  fs.mkdir(path, (err) => {
    if (err) {
      return console.error('Error creating folder:', err);
    }
    console.log("Successfully created folder");

    for (let i = 1; i <= numberOfFiles; i++) {
      fs.writeFile(`${filePath}-${i}.json`, jsonContent, (err) => {
        if (err) {
          console.error('Error creating file:', err);
          return;
        }
        console.log(`Successfully created file: ${filePath}-${i}`);

        callback(`${filePath}-${i}.json`);
      });
    }
  });
}

function toDeleteFile(filePath) {
  fs.unlink(filePath, (err) => {
    if (err) {
      console.error(`Error deleting file: ${filePath}`, err);
    } else {
      console.log(`File deleted successfully: ${filePath}`);
    }
  });
}

module.exports = {toCreateFolderWithFile,toDeleteFile}


