const fs = require('fs');
let fileName = 'lipsum.txt';

function readDataFromTxt(fileName) {
    fs.readFile(fileName, 'utf8', (err, data) => {
        if (err) throw err;
        let value = data;
        convertIntoUpperCase(value);
    });
}



function convertIntoUpperCase(value) {
    let upperCaseContent = value.toUpperCase();
    let upperCaseFileName = 'uppercase.txt';
    fs.writeFile(upperCaseFileName, upperCaseContent, (err) => {
        if (err) throw err;
        else {
            console.log("Uppercase file has been created");
        }
        fs.appendFile('filenames.txt', upperCaseFileName + '\n', (err) => {
            if (err) throw err;
        });

        toConvertUpperCasetoLowerCase(upperCaseFileName);
    });
}


function toConvertUpperCasetoLowerCase(upperCaseFileName) {
    fs.readFile(upperCaseFileName, 'utf8', (err, data) => {
        if (err) throw err;

        const lowerCaseContent = data.toLowerCase();
        const lowerCaseFileName = 'lowercase.txt';
        const sentences = lowerCaseContent.split('. ');

        fs.writeFile(lowerCaseFileName, sentences.join('.\n'), (err) => {
            if (err) throw err;

            fs.appendFile('filenames.txt', lowerCaseFileName + '\n', (err) => {
                if (err) throw err;

                fs.readFile(upperCaseFileName, 'utf8', (err, upperCaseData) => {
                    if (err) throw err;

                    fs.readFile(lowerCaseFileName, 'utf8', (err, lowerCaseData) => {
                        if (err) throw err;

                        const sortedContent = upperCaseData + '\n' + lowerCaseData;
                        const sortedFileName = 'sorted.txt';


                        const sortedSentences = sortedContent.split('. ').sort().join('.\n');

                        fs.writeFile(sortedFileName, sortedSentences, (err) => {
                            if (err) throw err;

                            fs.appendFile('filenames.txt', sortedFileName + '\n', (err) => {
                                if (err) throw err;
                                console.log('All tasks completed successfully!');

                                deleteFiles(upperCaseFileName);
                                deleteFiles(lowerCaseFileName);
                                deleteFiles(sortedFileName);
                            });
                        });
                    });
                });
            });
        });
    });
}

function deleteFiles(file) {
    fs.unlink(file, (err) => {
        if (err) throw err;
        console.log(`${file} file deleted.`);
    });
}


module.exports = readDataFromTxt